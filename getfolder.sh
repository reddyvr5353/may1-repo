#! /bin/bash
folder=`git diff --name-only --diff-filter=ADMR @~..@ | awk 'BEGIN {FS="/"} {print $1}' | uniq`
echo $folder
if [ "$folder" = "folder" ]
then
cd folder1
ls
else
cd folder2
ls
fi
pwd
chmod 777 build.sh
./build.sh